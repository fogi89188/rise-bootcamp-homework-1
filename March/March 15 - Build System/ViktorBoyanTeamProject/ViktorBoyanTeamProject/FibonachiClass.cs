﻿using System;

namespace ViktorBoyanTeamProject
{
    public class FibonachiClass
    {
        public static void Main()
        {

        }

        public bool CheckFib(int number)
        {
            if (number < 0)
            {
                return false;
            }
            if (number == 0)
            {
                return true;
            }

            //Mathematical Formula to check for fibonachi number
            double case1 = Math.Sqrt(5 * number * number + 4);
            double case2 = Math.Sqrt(5 * number * number - 4);

            int caseResult1 = (int)Math.Ceiling(Math.Sqrt(5 * number * number + 4));
            int caseResult2 = (int)Math.Ceiling(Math.Sqrt(5 * number * number - 4));

            if (caseResult1 == case1 || caseResult2 == case2)
                return true;
            else
                return false;
        }

        public int GetNextFib(int number)
        {
            if (CheckFib(number) == false)
            {
                return -1;
            }
            if (number == 0)
            {
                return 1;
            }

            //Mathematical Formula to get next fibonachi number
            double result = number * (1 + Math.Sqrt(5)) / 2;

            return (int)Math.Round(result);
        }

        public bool CheckArrayForFibonachiSequence(int[] array)
        {
            int numberOneEncounters = 0;
            if (array == null || array.Length <= 0)
            {
                return false;
            }
            for(int i = 0; i< array.Length; i++)
            {
                if (!CheckFib(array[i]))
                {
                    return false;
                }
                if (array[i] == 1)
                {
                    numberOneEncounters++;
                }
                if (i != 0)
                {
                    if (numberOneEncounters != 2)
                    {
                        if (array[i] != GetNextFib(array[i - 1]))
                        {
                            return false;
                        }
                    }
                }
            }

            return true;
        }
    }
}