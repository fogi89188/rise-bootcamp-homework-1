﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViktorBoyanTeamProject;

namespace TestFibonacciMatrices
{
    [TestClass]
    public  class TestFibonachiSequence
    {
        [TestMethod]
        public void TestFibonachiGivenCorrectArray1()
        {
            FibonachiClass fibonachi = new FibonachiClass();
            int[] testArr = { 1, 1, 2, 3, 5, 8 };

            Assert.IsTrue(fibonachi.CheckArrayForFibonachiSequence(testArr));
        }
        [TestMethod]
        public void TestFibonachiGivenCorrectArray2()
        {
            FibonachiClass fibonachi = new FibonachiClass();
            int[] testArr = { 1, 2, 3 };

            Assert.IsTrue(fibonachi.CheckArrayForFibonachiSequence(testArr));
        }
        [TestMethod]
        public void TestFibonachiGivenCorrectArray3()
        {
            FibonachiClass fibonachi = new FibonachiClass();
            int[] testArr = { 0, 1, 1, 2, 3 };

            Assert.IsTrue(fibonachi.CheckArrayForFibonachiSequence(testArr));
        }
        [TestMethod]
        public void TestFibonachiGivenNull()
        {
            FibonachiClass fibonachi = new FibonachiClass();
            int[] testArr = null;

            Assert.IsFalse(fibonachi.CheckArrayForFibonachiSequence(testArr));
        }
        [TestMethod]
        public void TestFibonachiGivenEmptyArray()
        {
            FibonachiClass fibonachi = new FibonachiClass();
            int[] testArr = { };

            Assert.IsFalse(fibonachi.CheckArrayForFibonachiSequence(testArr));
        }
        [TestMethod]
        public void TestFibonachiGivenIncorrectArray1()
        {
            FibonachiClass fibonachi = new FibonachiClass();
            int[] testArr = { 1, 1, 1, 4, 6, 8 };

            Assert.IsFalse(fibonachi.CheckArrayForFibonachiSequence(testArr));
        }
        [TestMethod]
        public void TestFibonachiGivenSingleElementArray()
        {
            FibonachiClass fibonachi = new FibonachiClass();
            int[] testArr = { 1 };

            Assert.IsTrue(fibonachi.CheckArrayForFibonachiSequence(testArr));
        }
    }
}
