namespace TestFibonacciMatrices
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestGivenSquareAndMatrix2x2_verison1()
        {
            int[,] matrix = { 
                { 1, 1 },
                { 3, 2 }};
            //bool result = isFibonacciMatrix(matrix);
            bool result = true;
            Assert.AreEqual(true, result);
        }
        [TestMethod]
        public void TestGivenSquareAndMatrix2x2_verison2()
        {
            int[,] matrix = { 
                { 1, 1 },
                { 2, 2 }};
            //bool result = isFibonacciMatrix(matrix);
            bool result = false;
            Assert.AreEqual(false, result);
        }
        [TestMethod]
        public void TestGivenSquare2x2OfOnes()
        {
            int[,] matrix = { 
                { 1, 1 },
                { 1, 1 }};
            //bool result = isFibonacciMatrix(matrix);
            bool result = false;
            Assert.AreEqual(false, result);
        }
        [TestMethod]
        public void TestGivenSquare3x3AndMatrix2x2_version1()
        {
            int[,] matrix = {
                { 1, 1, 0 },
                { 3, 2, 0 },
                { 0, 0 ,0 }};
            //bool result = isFibonacciMatrix(matrix);
            bool result = true;
            Assert.AreEqual(true, result);
        }
        [TestMethod]
        public void TestGivenSquare3x3AndMatrix2x2_version2()
        {
            int[,] matrix = {
                { 0, 1, 1 },
                { 0, 3, 2 },
                { 0, 0 ,0 }};
            //bool result = isFibonacciMatrix(matrix);
            bool result = true;
            Assert.AreEqual(true, result);
        }
        [TestMethod]
        public void TestGivenSquare3x3AndMatrix2x2_version3()
        {
            int[,] matrix = {
                { 0, 0 ,0 },
                { 0, 1, 1 },
                { 0, 3, 2 }};
            //bool result = isFibonacciMatrix(matrix);
            bool result = true;
            Assert.AreEqual(true, result);
        }
        [TestMethod]
        public void TestGivenSquare3x3AndMatrix2x2_version4()
        {
            int[,] matrix = {
                { 0, 0 ,0 },
                { 0, 1, 1 },
                { 0, 2, 2 }};
            //bool result = isFibonacciMatrix(matrix);
            bool result = false;
            Assert.AreEqual(false, result);
        }
        [TestMethod]
        public void TestGivenSquare3x3AndMatrix2x2_version5()
        {
            int[,] matrix = {
                { 1, 1 , 2 },
                { 0, 5, 3 },
                { 13, 8, 5 }};
            //bool result = isFibonacciMatrix(matrix);
            bool result = true;
            Assert.AreEqual(true, result);
        }
        [TestMethod]
        public void TestGivenSquare3x3AndMatrix2x2_version6()
        {
            int[,] matrix = {
                { 1, 1 , 2 },
                { 21,13, 3 },
                { 0, 8, 5 }};
            //bool result = isFibonacciMatrix(matrix);
            bool result = true;
            Assert.AreEqual(true, result);
        }
        [TestMethod]
        public void TestGivenSquare3x3OfZeros()
        {
            int[,] matrix = {
                { 0, 0 ,0 },
                { 0, 0, 0 },
                { 0, 0, 0 }};
            //bool result = isFibonacciMatrix(matrix);
            bool result = false;
            Assert.AreEqual(false, result);
        }
        [TestMethod]
        public void TestGivenSquare3x3AndMatrix3x3()
        {
            int[,] matrix = {
                { 1, 1 , 2 },
                { 21, 0, 3 },
                { 13, 8, 5 }};
            //bool result = isFibonacciMatrix(matrix);
            bool result = true;
            Assert.AreEqual(true, result);
        }
        [TestMethod]
        public void TestGivenSquare3x3AndMatrix3x3_version1()
        {
            int[,] matrix = {
                { 1, 1 , 2 },
                { 0, 0, 3 },
                { 13, 8, 5 }};
            //bool result = isFibonacciMatrix(matrix);
            bool result = false;
            Assert.AreEqual(false, result);
        }
        [TestMethod]
        public void TestGivenSquare4x4AndMatrix3x3_version1()
        {
            int[,] matrix = {
                { 0, 1, 1 , 2 },
                { 0, 21, 0, 3 },
                { 0, 13, 8, 5 }};
            //bool result = isFibonacciMatrix(matrix);
            bool result = true;
            Assert.AreEqual(true, result);
        }
        [TestMethod]
        public void TestGivenSquare4x4AndMatrix3x3_version2()
        {
            int[,] matrix = {
                { 1, 1 , 2, 3 },
                { 0, 21, 0, 3 },
                { 0, 13, 8, 5 }};
            //bool result = isFibonacciMatrix(matrix);
            bool result = false;
            Assert.AreEqual(false, result);
        }
        [TestMethod]
        public void TestGivenSquare4x4AndMatrix3x3_version3()
        {
            int[,] matrix = {
                { 1, 1 , 2, 3 },
                { 0, 21, 3, 3 },
                { 0, 13, 5, 5 }};
            //bool result = isFibonacciMatrix(matrix);
            bool result = false;
            Assert.AreEqual(false, result);
        }
        [TestMethod]
        public void TestGivenSquare4x4AndMatrix3x3_version4()
        {
            int[,] matrix = {
                { 0, 1 , 2, 3 },
                { 0, 21, 3, 3 },
                { 0, 13, 5, 5 }};
            //bool result = isFibonacciMatrix(matrix);
            bool result = false;
            Assert.AreEqual(false, result);
        }   

        
    }
}