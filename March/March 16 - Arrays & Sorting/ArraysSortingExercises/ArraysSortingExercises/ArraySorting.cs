﻿using System;
using System.Security.Cryptography;

namespace ArraysSortingExercisesProject
{
    public static class ArraySorting
    {
        public static void Main()
        {

            int[] testArray = new int[] { 9, 7, 8, 6, 5, 4, 3, 2, 0 };

            Console.WriteLine(FindMissing(MergeSort(testArray)));

            FindRoot3(3);
        }

        public static int[] MergeSort(int[] array)
        {
            if (array == null)
            {
                throw new ArgumentNullException();
            }
            MergeSort(array, 0, array.Length - 1);
            return array;
        }
        private static int[] MergeSort(int[] array, int left, int right)
        {
            if (left < right)
            {
                int middle = left + (right - left) / 2;

                MergeSort(array, left, middle);
                MergeSort(array, middle + 1, right);

                MergeArray(array, left, middle, right);
            }

            return array;
        }
        private static void MergeArray(int[] array, int left, int middle, int right)
        {
            var leftArrayLength = middle - left + 1;
            var rightArrayLength = right - middle;
            var leftTempArray = new int[leftArrayLength];
            var rightTempArray = new int[rightArrayLength];
            int i, j;

            for (i = 0; i < leftArrayLength; ++i)
                leftTempArray[i] = array[left + i];
            for (j = 0; j < rightArrayLength; ++j)
                rightTempArray[j] = array[middle + 1 + j];

            i = 0;
            j = 0;
            int k = left;

            while (i < leftArrayLength && j < rightArrayLength)
            {
                if (leftTempArray[i] <= rightTempArray[j])
                {
                    array[k++] = leftTempArray[i++];
                }
                else
                {
                    array[k++] = rightTempArray[j++];
                }
            }

            while (i < leftArrayLength)
            {
                array[k++] = leftTempArray[i++];
            }

            while (j < rightArrayLength)
            {
                array[k++] = rightTempArray[j++];
            }
        }

        public static int FindMissing(int[] array)
        {
            if (array.Length <= 1 || array == null)
            {
                return 0;
            }
            if (array[0] != array[1] - 1)
            {
                return array[0] + 1;
            }
            for (int i = 1; i < array.Length - 1; i++)
            {
                if (array[i] != array[i + 1] - 1)
                {
                    return array[i] + 1;
                }
            }

            return 0;
        }

        public static int FindRoot3(int input)
        {
            //input will always be a perfect input so result will never have decimal spaces
            if (input == 0)
            {
                return 0;
            }
            else if (input < 0)
            {
                throw new Exception("You cannot find the cubic root of a negative number.");
            }

            //Using binary search to find sqrt3
            float start = 0f;
            float end = input;

            while (true)
            {
                float midPoint = (start + end) / 2;
                if (midPoint * midPoint * midPoint == input)
                {
                    return (int)midPoint;
                }
                if (midPoint * midPoint * midPoint > input)
                {
                    end = midPoint;
                }
                else
                {
                    start = midPoint;
                }
            }
        }
    }
}