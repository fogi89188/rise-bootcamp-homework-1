﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ArraysSortingExercisesProject;

namespace TestingArraysProject
{
    [TestClass]
    public class TestFindRoot3
    {
        [TestMethod]
        public void TestRoot3Given0()
        {
            int testNum = 0;
            int expected = 0;

            int result = ArraySorting.FindRoot3(testNum);

            Assert.AreEqual(expected, result);
        }
        [TestMethod]
        public void TestRoot3Given1()
        {
            int testNum = 1;
            int expected = 1;

            int result = ArraySorting.FindRoot3(testNum);

            Assert.AreEqual(expected, result);
        }
        [TestMethod]
        public void TestRoot3Given8()
        {
            int testNum = 8;
            int expected = 2;

            int result = ArraySorting.FindRoot3(testNum);

            Assert.AreEqual(expected, result);
        }
        [TestMethod]
        public void TestRoot3Given27()
        {
            int testNum = 27;
            int expected = 3;

            int result = ArraySorting.FindRoot3(testNum);

            Assert.AreEqual(expected, result);
        }
        [TestMethod]
        public void TestRoot3Given2744()
        {
            int testNum = 2744;
            int expected = 14;

            int result = ArraySorting.FindRoot3(testNum);

            Assert.AreEqual(expected, result);
        }
        [TestMethod]
        public void TestRoot3GivenNegativeNum()
        {
            int testNum = -3;

            try
            {
                int result = ArraySorting.FindRoot3(testNum);
            }
            catch
            {
                Assert.IsTrue(true);
            }
        }
    }
}
