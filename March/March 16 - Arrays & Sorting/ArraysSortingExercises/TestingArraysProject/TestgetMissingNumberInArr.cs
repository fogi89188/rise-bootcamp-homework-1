﻿using ArraysSortingExercisesProject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestingArraysProject
{
    [TestClass]
    public class TestgetMissingNumberInArr
    {
        [TestMethod]
        public void TestMissingNumGivenMiddleMissing()
        {
            int[] testArray = new int[] { 9, 8, 7, 6, 4, 3, 2, 1, 0 };
            int expected = 5;

            int[] sortedArray = ArraySorting.MergeSort(testArray);
            int missingNumber = ArraySorting.FindMissing(sortedArray);

            Assert.IsNotNull(sortedArray);
            Assert.AreEqual(missingNumber, expected);
        }
        [TestMethod]
        public void TestMissingNumGivenLeftMissing()
        {
            int[] testArray = new int[] { 9, 7, 6, 5, 4, 3, 2, 1, 0 };
            int expected = 8;

            int[] sortedArray = ArraySorting.MergeSort(testArray);
            int missingNumber = ArraySorting.FindMissing(sortedArray);

            Assert.IsNotNull(sortedArray);
            Assert.AreEqual(missingNumber, expected);
        }
        [TestMethod]
        public void TestMissingNumGivenRightMissing()
        {
            int[] testArray = new int[] { 9, 7, 8, 6, 5, 4, 3, 2, 0 };
            int expected = 1;

            int[] sortedArray = ArraySorting.MergeSort(testArray);
            int missingNumber = ArraySorting.FindMissing(sortedArray);

            Assert.IsNotNull(sortedArray);
            Assert.AreEqual(missingNumber, expected);
        }
        [TestMethod]
        public void TestMissingNumGivenEmptyArray()
        {
            int[] testArray = new int[] { };
            int expected = 0;

            int[] sortedArray = ArraySorting.MergeSort(testArray);
            int missingNumber = ArraySorting.FindMissing(sortedArray);

            Assert.IsNotNull(sortedArray);
            Assert.AreEqual(missingNumber, expected);
        }
        [TestMethod]
        public void TestMissingNumGivenNullArray()
        {
            int[] testArray = null;

            try
            {
                int[] sortedArray = ArraySorting.MergeSort(testArray);
                int missingNumber = ArraySorting.FindMissing(sortedArray);
            }
            catch
            {
                Assert.IsTrue(true);
            }
        }
    }
}
