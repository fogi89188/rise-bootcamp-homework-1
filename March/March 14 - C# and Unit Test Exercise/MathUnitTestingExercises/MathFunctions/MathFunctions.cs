﻿using System;
using System.Collections.Immutable;

namespace MathFunctions // Note: actual namespace depends on the project name.
{
    public class MathFunctions
    {
        static void Main(string[] args)
        {

        }

        public int CheckLimitOfPrimitiveType()
        {
            int maxInt = int.MaxValue;
            maxInt += 1;
            return maxInt;
        }
        public bool IsOdd(int num)
        {
            if (num % 2 == 1)
            {
                return true;
            }
            return false;
        }
        public bool IsPrime(int num)
        {
            if (num == 0 || num == 1) // 0 and 1 arent prime numebrs
            {
                return false;
            }
            for (int i = 2; i <= num / 2; ++i)
            {
                if (num % i == 0)
                {
                    return false;
                }
            }
            return true;
        }
        public int MinElementInArray(int[] array)
        {
            if (array.Length <= 0)
            {
                throw new ArgumentOutOfRangeException("Given array cannot have 0 lenght.");
            }
            int min = int.MaxValue;
            foreach(int element in array)
            {
                if (element < min)
                {
                    min = element;
                }
            }
            return min;
        }
        public int KthMinElementInArray(int k, int[] array)
        {
            if (array.Length <= 0)
            {
                throw new ArgumentOutOfRangeException("Given array cannot have 0 lenght.");
            }
            if (k == 0)
            {
                throw new ArgumentOutOfRangeException("K-th element cannot be 0.");
            }
            if (k > array.Length)
            {
                throw new ArgumentOutOfRangeException("Cannot search for Kth element larger than size of array.");
            }

            int min = int.MaxValue;

            Array.Sort(array);
            min = array[k-1];

            return min;
        }
        public int GetOddOccurance(int[] array)
        {
            if (array.Length <= 0)
            {
                throw new ArgumentOutOfRangeException("Given array cannot have 0 lenght.");
            }

            Dictionary<int, int> occurances = new Dictionary<int, int>();

            foreach (int num in array)
            {
                if (occurances.ContainsKey(num))
                {
                    occurances[num]++;
                }
                else
                {
                    occurances[num] = 1;
                }
            }

            foreach (int num in array)
            {
                if (occurances[num] % 2 == 1)
                {
                    return num;
                }
            }

            // If no number has an odd count, return -1 or throw an exception
            throw new Exception("No number has only a single occurance");
        }
        public int GetAverage(int[] array)
        {
            if (array.Length <= 0)
            {
                throw new ArgumentOutOfRangeException("Given array cannot have 0 lenght.");
            }

            int sum = 0;

            foreach (int element in array)
            {
                sum += element;
            }

            return sum/array.Length;
        }
        public long Pow(int number, int power)
        {
            if (power == 0)
            {
                return 1;
            }
            else if (power % 2 == 0)
            {
                long temp = Pow(number, power / 2);
                return temp * temp;
            }
            else
            {
                long temp = Pow(number, (power - 1) / 2);
                return (long)number * temp * temp;
            }
        }
    }
}