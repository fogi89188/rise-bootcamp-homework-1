﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathUnitTestingExercises
{
    [TestClass]
    public class TestGetOddOccurance
    {
        [TestMethod]
        public void TestGetOddOccuranceGivenZeroArray()
        {
            //Arrange
            MathFunctions.MathFunctions math = new MathFunctions.MathFunctions();
            int[] testArr = { };
            //Act
            try
            {
                math.GetOddOccurance(testArr);
            }
            //Assert
            catch
            {
                Assert.IsTrue(true);
            }
        }
        [TestMethod]
        public void TestGetOddOccuranceGivenOnlyEven()
        {
            //Arrange
            MathFunctions.MathFunctions math = new MathFunctions.MathFunctions();
            int[] testArr = {1, 1, 2, 2, 3, 3, 4, 4 };
            //Act
            try
            {
                math.GetOddOccurance(testArr);
            }
            //Assert
            catch
            {
                Assert.IsTrue(true);
            }
        }
        [TestMethod]
        public void TestGetOddOccuranceGivenArray1()
        {
            //Arrange
            MathFunctions.MathFunctions math = new MathFunctions.MathFunctions();
            int[] testArr = { 1, 2, 3, 4, 5 };
            bool result;

            //Act
            if (math.GetOddOccurance(testArr) == 1) result = true;
            else result = false;

            //Assert
            Assert.IsTrue(result);
        }
        [TestMethod]
        public void TestGetOddOccuranceGivenArray2()
        {
            //Arrange
            MathFunctions.MathFunctions math = new MathFunctions.MathFunctions();
            int[] testArr = { 1, 1, 3, 4, 5 };
            bool result;

            //Act
            if (math.GetOddOccurance(testArr) == 3) result = true;
            else result = false;

            //Assert
            Assert.IsTrue(result);
        }
        [TestMethod]
        public void TestGetOddOccuranceGivenArray3()
        {
            //Arrange
            MathFunctions.MathFunctions math = new MathFunctions.MathFunctions();
            int[] testArr = { 1, 1, 3, 4, 3, 5, 4, 5, 1, 5, 1 };
            bool result;

            //Act
            if (math.GetOddOccurance(testArr) == 5) result = true;
            else result = false;

            //Assert
            Assert.IsTrue(result);
        }
    }
}
