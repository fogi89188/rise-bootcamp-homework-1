﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathUnitTestingExercises
{
    [TestClass]
    public class TestGetAverage
    {
        [TestMethod] 
        public void TestGetAverageGivenZeroArray()
        {
            //Arrange
            MathFunctions.MathFunctions math = new MathFunctions.MathFunctions();
            int[] testArr = { };
            //Act
            try
            {
                math.GetAverage(testArr);
            }
            //Assert
            catch
            {
                Assert.IsTrue(true);
            }
        }
        [TestMethod]
        public void TestGetAverageGivenArray1()
        {
            //Arrange
            MathFunctions.MathFunctions math = new MathFunctions.MathFunctions();
            int[] testArr = { 8 };
            bool result;

            //Act
            if (math.GetAverage(testArr) == 8) result = true;
            else result = false;

            //Assert
            Assert.IsTrue(result);
        }
        [TestMethod]
        public void TestGetAverageGivenArray2()
        {
            //Arrange
            MathFunctions.MathFunctions math = new MathFunctions.MathFunctions();
            int[] testArr = { 2, 2, 2 };
            bool result;

            //Act
            if (math.GetAverage(testArr) == 2) result = true;
            else result = false;

            //Assert
            Assert.IsTrue(result);
        }
        [TestMethod]
        public void TestGetAverageGivenArray3()
        {
            //Arrange
            MathFunctions.MathFunctions math = new MathFunctions.MathFunctions();
            int[] testArr = { 1, 2, 3, 4, 5 };
            bool result;

            //Act
            if (math.GetAverage(testArr) == 3) result = true;
            else result = false;

            //Assert
            Assert.IsTrue(result);
        }
        [TestMethod]
        public void TestGetAverageGivenArray4()
        {
            //Arrange
            MathFunctions.MathFunctions math = new MathFunctions.MathFunctions();
            int[] testArr = { 1, 1, 2, 3, 4, 5 };
            bool result;

            //Act
            if (math.GetAverage(testArr) == 2) result = true;
            else result = false;

            //Assert
            Assert.IsTrue(result);
        }
    }
}
