﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathUnitTestingExercises
{
    [TestClass]
    public class TestPow
    {
        [TestMethod]
        public void TestPowGivenZero()
        {
            //Arrange
            MathFunctions.MathFunctions math = new MathFunctions.MathFunctions();

            //Act & Assert
            Assert.AreEqual(1, math.Pow(1, 0));
        }
        [TestMethod]
        public void TestPowGiven1Pow2()
        {
            //Arrange
            MathFunctions.MathFunctions math = new MathFunctions.MathFunctions();

            //Act & Assert
            Assert.AreEqual(1, math.Pow(1, 2));
        }
        [TestMethod]
        public void TestPowGiven2Pow2()
        {
            //Arrange
            MathFunctions.MathFunctions math = new MathFunctions.MathFunctions();

            //Act & Assert
            Assert.AreEqual(4, math.Pow(2, 2));
        }
        [TestMethod]
        public void TestPowGivenIntMaxValPow2()
        {
            //Arrange
            MathFunctions.MathFunctions math = new MathFunctions.MathFunctions();
            long result = 0;

            //Act
            result = math.Pow(int.MaxValue, 2);

            //Assert
            if (result > int.MaxValue)
            {
                Assert.IsTrue(true);
            }
            else
            {
                Assert.IsTrue(false);
            }
        }
    }
}
